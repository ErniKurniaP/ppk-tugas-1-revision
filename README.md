# Tugas 1 Pemrograman Platform Khusus
# Menampilkan RSS Berita dari Berbagai Sumber (Tempo, CNNIndonesia, Republika.co.id, dan BMKG) Menggunakan Web Sederhana

Nama	        : Erni Kurnia Putri
<br /> NIM	    : 222011792
<br /> Kelas	: 3SI3
<br /> No       : 29

Mohon maaf Pak, file tugas 1 yang saya kumpulkan beberapa waktu lalu menggunakan CodeIgniter 3 dan ketika saya deploy bermasalah pada composernya sehingga saya beralih ke CodeIgniter 4. Berikut link web yang berhasil saya deploy : https://ppk-tugas1-ernikurniaputri.herokuapp.com/
